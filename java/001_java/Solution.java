import java.util.Scanner;
public class Solution {
	static/* public int[] twoSum(int[] nums, int target) {
		int[] res=new int[2];
		for (int i=0;i<nums.length;i++){
			for (int j=i+1;j<nums.length;j++){
				if ((nums[i]+nums[j])==target) {
					res[0]=i+1;
					res[1]=j+1;
					return res;
				}
			}
		}
		return res;
    }*/
	public int[] twoSum(int[] nums, int target) {
		
		int[] res=new int[2];
		int i=0,j=nums.length-1;
		while (i!=j) {
			if (nums[i]+nums[j]>target){
				j--;
			}else if (nums[i]+nums[j]<target) {
				i++;
			}else{
				res[0]=i+1;
				res[1]=j+1;
				return res;
			}
		}
		return res;
	}
	public static void main(String [ ] args)
	{
		Scanner in = new Scanner(System.in);
		
		int l=in.nextInt();
		int[] ipt=new int[l];
		for (int i=0;i<l;i++){
			ipt[i]=in.nextInt();
		}
		int t=in.nextInt();
		
		int[] ans=twoSum(ipt,t);
		int a=ans[0];
		int b=ans[1];
		System.out.println(a+" "+b);
		
	}
	
}
