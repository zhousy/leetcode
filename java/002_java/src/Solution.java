
public class Solution {
	
	static public class ListNode {
		      int val;
		      ListNode next;
		      ListNode(int x) { val = x; }
		      ListNode(int x, ListNode y) { val = x; next=y;}
		  }
	
	static public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode l11=l1,l22=l2;
		ListNode res=null;
		ListNode temp=null;
		int jw=0;
		
	ListNode head=null;
		
	while ((l11!=null)&&(l22!=null)) {
		temp=new ListNode((l11.val+l22.val+jw)%10);
		jw=(l11.val+l22.val+jw)/10;
		if (res==null){
			res=temp;
			head=res;
		}else{
			res.next=temp;
			res=temp;
		}
		l11=l11.next; 
		l22=l22.next;
		//System.out.println(2);
	}
	while (l11!=null) {
		temp=new ListNode((l11.val+jw)%10);
		jw=(l11.val+jw)/10;
		res.next=temp;
		res=temp;
		l11=l11.next;
	}
	while (l22!=null){
		temp=new ListNode((l22.val+jw)%10);
		jw=(l22.val+jw)/10;
		res.next=temp;
		res=temp;
		l22=l22.next;
	}
	if (jw>0) {
		res.next=new ListNode(jw);
	}
	if (res.val>9){
		res.next=new ListNode(res.val/10);
		res.val=res.val%10;
	}
		return head;
    }

	public static void main(String[] args) {
		ListNode a=new ListNode(8,new ListNode(9,new ListNode(9)));
		ListNode b=new ListNode(2);
		ListNode c=addTwoNumbers(a,b);
		while (c!=null){
			System.out.println(c.val);
			c=c.next;
		}
	}

}
